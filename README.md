# Ansible role: labocbz.install_apacheds

![Licence Status](https://img.shields.io/badge/licence-MIT-brightgreen)
![Testing Method](https://img.shields.io/badge/Testing%20Method-Ansible%20Molecule-blueviolet)
![Testing Driver](https://img.shields.io/badge/Testing%20Driver-docker-blueviolet)
![Language Status](https://img.shields.io/badge/language-Ansible-red)
![Compagny](https://img.shields.io/badge/Compagny-Labo--CBZ-blue)
![Author](https://img.shields.io/badge/Author-Lord%20Robin%20Crombez-blue)

## Description

![Tag: Ansible](https://img.shields.io/badge/Tech-Ansible-orange)
![Tag: Debian](https://img.shields.io/badge/Tech-Debian-orange)
![Tag: Ubuntu](https://img.shields.io/badge/Tech-Ubuntu-orange)
![Tag: ApacheDS](https://img.shields.io/badge/Tech-ApacheDS-orange)

An Ansible role to install and configure an ApacheDS server on your host.

This Ansible role facilitates the deployment and customization of Apache Directory Server. It enables the creation and configuration of multiple LDAP instances based on the default one on a single machine through adjustable variables including version, installation directory, user, and group.

Configuration flexibility is achieved using LDIF files. Enhanced security is provided by automatic password strengthening, while system services are generated for efficient management. Simplify the installation and management of LDAP instances using this versatile role.

## Usage

```SHELL
# Install husky and init
npm i && npx husky init && npm i validate-branch-name && npm cz

# Initialize the local secrets database, if not already present
MSYS_NO_PATHCONV=1 docker run -it --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest detect-secrets scan --exclude-files 'node_modules' > .secrets.baseline

# Decrypt the local Ansible Vault
# Get the Ansible Local Vault Key on PassBolt and create the .ansible-vault.key file, after that you can unlock your local vault
MSYS_NO_PATHCONV=1 docker run --rm -it -v "$HOME/.docker:/root/.docker" -v "$(pwd):/root/ansible/${DIR}" -w /root/ansible/${DIR} labocbz/ansible-molecule:latest /bin/sh -c 'cat ./.ansible-vault.key > /tmp/.ansible-vault.key && chmod -x /tmp/.ansible-vault.key && ansible-vault decrypt --vault-password-file /tmp/.ansible-vault.key ./tests/inventory/group_vars/vault.yml'
```

### Linters

```SHELL
# Analyse the project for secrets, and audit
./detect-secrets

# Lint Markdown
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest markdownlint './**.md' --disable MD013

# Lint YAML
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest yamllint -c ./.yamllint .

# Lint Ansible
MSYS_NO_PATHCONV=1 docker run --rm -v "$(pwd):/app" -w /app labocbz/devops-linters:latest ansible-lint --offline --exclude node_modules -x meta-no-info -p .
```

### Local Tests

This repository contain a unique Molecule controller to start containers and tests your roles and playbook inside them.

```SHELL
# Start the Molecule controller
./molecule-controller

# Inside the controller
molecule create
molecule converge
molecule verify
molecule test
```

### Use

In order to use this role, you have to know its defaults vars and their purppose.

### Vars

This role have these default vars here: [defaults.yml](./defaults/main.yml).

### Import and Run

To run this role or use it inside a playbook, import the call task from [converge.yml](./molecule/default/converge.yml).

## Architectural Decisions Records

Here you can put your change to keep a trace of your work and decisions.

### 2023-08-21: First Init

* First init of this role with the bootstrap_role playbook by Lord Robin Crombez

### 2023-08-23: Started and default instance tested

* Role can now install and copy the default instance to another one
* No changement in the configuration for now
* Tested with ldapsearch command

### 2023-08-24: SSL Available and fixs

* Role can now create a JKS for SSL remote access
* You can now deploy a default instance and then do your stuff inside
* Added some fix about lint

### 2023-08-25: Final version

* Remove JKS/P12 creation because user have to provide it after
* Based on the default LDIF, multiple instance will not start (port un use), so if multiple instance is wanted, you have to configure the first, relaunch the role and supress the first entry, configure the second, etc
* Default password have been changed on run with LDAP base command, the service will to be up for that
* Role create systemd services based on the instance name, like "apacheds_local"

### 2023-09-16: Multipe instance install

* Role now support install multiple instance and set different ports
* Work for SSL in progress
* Work for partition change in progress
* Modification have to be on the verify first server statup, so any modification after is not possible

### 2023-09-1b: No automatic SSL/TLS

* Role dont change / edit the default JKS used by ApacheDS
* Role create a JKS and P12 file with a defined password, you have to edit the configuration after the first start
* Dont forget to disable Anonymous Access
* If SSL/TLS not setted, role disable the default LDAPS endpoint
* Role dont edit que default partition

### 2023-10-06: New CICD, new Images

* New CI/CD scenario name
* Molecule now use remote Docker image by Lord Robin Crombez
* Molecule now use custom Docker image in CI/CD by env vars
* New CICD with needs and optimization

### 2023-12-14: System users

* Role can now use system users and address groups

### 2024-02-02: Multi instance and SSL

* Multi instance doesn't support port edition (can be broken on install / configuration)
* SSL/TLS materials are prepared, but you have to configure your install to use them
* Use latest version of ApacheDS 27

### 2024-02-22: New CICD and fixes

* Added support for Ubuntu 22
* Added support for Debian 11/22
* Edited vars for linting (role name and __)
* Added generic support for Docker dind (can add used for obscures reasons ... user in use)
* Fix idempotency
* Added informations for UID and GID for user/groups
* Added support for user password creation (on_create)
* New CI, need work on tag and releases
* CI use now Sonarqube

### 2024-05-19: New CI

* Added Markdown lint to the CICD
* Rework all Docker images
* Change CICD vars convention
* New workers
* Removed all automation based on branch

### 2024-10-27: Global refactoring

* Added one local Ansible Vault
* Edited gitignore file
* Add some commands in documentation
* Refactored the role
* Added automations to build LDAP structure

### 2024-12-31: New CICD and images

* Edited all Docker images
* Rework on the CICD
* Enabled SonarQube

### 2025-01-05: Certificates update

* Edit for use the latest version of import_certificates

## Authors

* Lord Robin Crombez

## Sources

* [Ansible role documentation](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
* [Ansible Molecule documentation](https://molecule.readthedocs.io/)
